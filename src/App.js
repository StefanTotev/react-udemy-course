import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person';

class App extends Component {
    state = {
        persons: [
            { name: 'Stefan', age: 26 },
            { name: 'Marin', age: 28 },
            { name: 'Valentina', age: 24 },
        ]
    };

    switchNameHandler = ( newName, newAge ) => {
        this.setState({
            persons: [
                { name: newName, age: newAge },
                { name: 'Marin', age: 28 },
                { name: 'Valentina', age: 24 },
            ]
        });
    };

    changeNameHandler = ( event ) => {
        this.setState({
            persons: [
                { name: event.target.value, age: 26 },
                { name: 'Marin', age: 28 },
                { name: 'Valentina', age: 24 },
            ]
        });
    };

    render() {
        const inlineStyle = {
            backgroundColor: '#fff',
            border: '1px solid red',
            padding: '5px'
        };

        return (
            <div className="App">
                <p>This is a paragraph!</p>
                <button
                    style={inlineStyle}
                    onClick={this.switchNameHandler.bind(this, 'Valentin', 30)}>Switch names</button>
                <Person
                    name={this.state.persons[0].name}
                    age={this.state.persons[0].age}
                    click={() => this.switchNameHandler('Ico', 23)}
                    changed={this.changeNameHandler}
                >My Hobbies: Web Programming</Person>
                <Person name={this.state.persons[1].name} age={this.state.persons[1].age} />
                <Person name={this.state.persons[2].name} age={this.state.persons[2].age} />
            </div>
        );
    }
}

export default App;
